package com.ever.asteroidradar.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ever.asteroidradar.Asteroid

@Database(entities = [NasaDailyImage::class, Asteroid::class], version = 5, exportSchema = false)
abstract class NasaDatabase : RoomDatabase() {
    abstract val nasaDatabaseDao: NasaDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: NasaDatabase? = null

        fun getInstance(context: Context): NasaDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        NasaDatabase::class.java,
                        "sleep_history_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
