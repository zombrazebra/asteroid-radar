package com.ever.asteroidradar.view.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ever.asteroidradar.Asteroid
import com.ever.asteroidradar.AsteroidUpdate
import com.ever.asteroidradar.database.NasaDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(private val nasaDatabase: NasaDatabase) : ViewModel() {

    lateinit var asteroid: LiveData<Asteroid>

    private val _favoriteStatus = MutableLiveData<Boolean?>()
    val favoriteStatus: LiveData<Boolean?>
        get() = _favoriteStatus

    fun updateFavoriteAsteroid(asteroid: Asteroid) {
        asteroid.isFavorite = !asteroid.isFavorite
        viewModelScope.launch(Dispatchers.IO) {
            val tempUpdate = AsteroidUpdate(asteroid.isFavorite, asteroid.id)
            nasaDatabase.nasaDatabaseDao.updateFavorite(tempUpdate)
            _favoriteStatus.postValue(asteroid.isFavorite)
        }
    }

    fun refreshAsteroid(id: Long) {
        asteroid = nasaDatabase.nasaDatabaseDao.getAsteroid(id)
    }
}

