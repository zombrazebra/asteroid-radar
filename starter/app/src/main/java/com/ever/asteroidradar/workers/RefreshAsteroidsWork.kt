package com.ever.asteroidradar.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.ever.asteroidradar.repository.NasaRepository
import java.lang.Exception

class RefreshAsteroidsWork(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    companion object {
        const val WORK_NAME = "RefreshAsteroids"
    }

    override suspend fun doWork(): Result {
        val repository = NasaRepository(context = applicationContext)

        return try {
            repository.clearYesterdayAsteroid()
            Result.success()
        } catch (e: Exception) {
            Result.retry()
        }
    }

}