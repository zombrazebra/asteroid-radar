package com.ever.asteroidradar.repository

enum class Status {
    LOADING, ERROR, DONE
}