package com.ever.asteroidradar.view.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ever.asteroidradar.database.NasaDatabase
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class DetailViewModelFactory(private val nasaDatabase: NasaDatabase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            return DetailViewModel(nasaDatabase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}