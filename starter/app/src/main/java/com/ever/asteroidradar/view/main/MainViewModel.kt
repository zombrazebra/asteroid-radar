package com.ever.asteroidradar.view.main

import androidx.lifecycle.*
import com.ever.asteroidradar.Asteroid
import com.ever.asteroidradar.repository.AsteroidFilter
import com.ever.asteroidradar.repository.NasaRepository
import kotlinx.coroutines.launch

class MainViewModel(private val nasaRepository: NasaRepository) : ViewModel() {

    val imageOfTheDay = nasaRepository.nasaImage
    val asteroidListStatus = nasaRepository.listStatus

    private val _asteroidList = nasaRepository.asteroidList
    val asteroidList: LiveData<List<Asteroid>> = _asteroidList
    private val _navigateToAsteroidDetails = MutableLiveData<Asteroid>()
    val navigateToAsteroidDetails: LiveData<Asteroid> = _navigateToAsteroidDetails

    fun refreshImageOfTheDay() {
        viewModelScope.launch {
            nasaRepository.getImageOfTheDay()
        }
    }

    fun refreshFilter(filter: AsteroidFilter) {
        viewModelScope.launch {
            nasaRepository.updateFilter(filter)
        }
    }

    fun displayDetailScreen(asteroid: Asteroid) {
        _navigateToAsteroidDetails.value = asteroid
    }

    fun doneNavigatingToDetail() {
        _navigateToAsteroidDetails.value = null
    }

    fun refreshList() {
        viewModelScope.launch {
            nasaRepository.refreshList()
        }
    }
}