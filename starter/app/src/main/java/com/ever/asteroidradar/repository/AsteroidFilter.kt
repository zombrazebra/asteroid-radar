package com.ever.asteroidradar.repository

enum class AsteroidFilter {
    ALL, TODAY, FAVORITE, NONE
}