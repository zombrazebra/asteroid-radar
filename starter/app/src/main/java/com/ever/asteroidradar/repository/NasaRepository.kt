package com.ever.asteroidradar.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ever.asteroidradar.Asteroid
import com.ever.asteroidradar.database.NasaDatabase
import com.ever.asteroidradar.network.NasaApi
import com.ever.asteroidradar.util.*
import kotlinx.coroutines.*
import org.json.JSONObject
import java.util.*


class NasaRepository(context: Context) {

    var filter: AsteroidFilter = AsteroidFilter.NONE
    private val database = NasaDatabase.getInstance(context)

    val nasaImage = database.nasaDatabaseDao.getDailyImage()
    val asteroidList = MutableLiveData<List<Asteroid>>()
    val listStatus: LiveData<Status>
        get() = _listStatus
    private val _listStatus = MutableLiveData<Status>()
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    init {
        scope.launch {
            refreshAsteroids()
        }
    }

    private suspend fun refreshAsteroids() {
        withContext(Dispatchers.IO) {
            try {
                _listStatus.postValue(Status.LOADING)
                val stringResponse = NasaApi.retrofitService.getAsteroids(
                    Calendar.getInstance().getCurrentDay(),
                    Calendar.getInstance().sumDaysToCurrent(),
                    Constants.API_KEY
                )
                if (stringResponse.isNotEmpty()) {
                    val asteroids = parseJson(JSONObject(stringResponse))
                    database.nasaDatabaseDao.insertAllAsteroids(asteroids)
                }
                _listStatus.postValue(Status.DONE)
            } catch (exception: Exception) {
                Log.e(NasaRepository::class.java.name, exception.message.toString())
                _listStatus.postValue(Status.ERROR)
            }
        }
        updateFilter(AsteroidFilter.ALL)
    }

    suspend fun getImageOfTheDay() {
        withContext(Dispatchers.IO) {
            if (nasaImage.value == null && nasaImage.value?.date != Calendar.getInstance()
                    .getCurrentDay()
            ) {
                try {
                    val newImage = NasaApi.retrofitService.getImageOfTheDay(Constants.API_KEY)
                    if (newImage.type == "image") {
                        database.nasaDatabaseDao.insertImage(newImage)
                    }
                } catch (exception: Exception) {
                    Log.e(NasaRepository::class.java.name, exception.message.toString())
                }
            }
        }
    }

    suspend fun refreshList() {
        updateFilter(filter)
    }

    suspend fun updateFilter(asteroidFilter: AsteroidFilter) {
        if (asteroidFilter != filter || asteroidFilter == AsteroidFilter.FAVORITE) {
            withContext(Dispatchers.IO) {
                when (asteroidFilter) {
                    AsteroidFilter.TODAY ->
                        asteroidList.postValue(
                            database.nasaDatabaseDao.getTodayAsteroids(
                                Calendar.getInstance().getCurrentDay()
                            )
                        )
                    AsteroidFilter.FAVORITE ->
                        asteroidList.postValue(
                            database.nasaDatabaseDao.getFavoriteAsteroids()
                        )
                    else ->
                        asteroidList.postValue(
                            database.nasaDatabaseDao.getWeekAsteroids(
                                Calendar.getInstance().getNextSevenDays()
                            )
                        )
                }
            }
        }
        filter = asteroidFilter
    }

    suspend fun clearYesterdayAsteroid() {
        withContext(Dispatchers.IO) {
            database.nasaDatabaseDao.clearYesterdayAsteroids(Calendar.getInstance().getYesterday())
        }
    }
}
