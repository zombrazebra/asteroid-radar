package com.ever.asteroidradar.util

import java.text.SimpleDateFormat
import java.util.*

fun Calendar.getCurrentDay(): String {
    val currentDay = this.time
    val dateFormat = SimpleDateFormat(Constants.API_QUERY_DATE_FORMAT, Locale.getDefault())
    return dateFormat.format(currentDay)
}

fun Calendar.getYesterday(): String {
    this.add(
        Calendar.DAY_OF_YEAR,
        -1
    )
    val currentDay = this.time
    val dateFormat = SimpleDateFormat(Constants.API_QUERY_DATE_FORMAT, Locale.getDefault())
    return dateFormat.format(currentDay)
}

fun Calendar.sumDaysToCurrent(): String {
    this.add(
        Calendar.DAY_OF_YEAR,
        Constants.DEFAULT_END_DATE_DAYS
    )
    val currentDay = this.time
    val dateFormat = SimpleDateFormat(Constants.API_QUERY_DATE_FORMAT, Locale.getDefault())
    return dateFormat.format(currentDay)
}

fun Calendar.getNextSevenDays(): MutableList<String> {
    val listOfDays = mutableListOf<String>()
    listOfDays.add(getCurrentDay())
    for (i in 1..7) {
        this.add(
            Calendar.DAY_OF_YEAR,
            1
        )
        val currentDay = this.time
        val dateFormat = SimpleDateFormat(Constants.API_QUERY_DATE_FORMAT, Locale.getDefault())
        listOfDays.add(dateFormat.format(currentDay))
    }

    return listOfDays
}