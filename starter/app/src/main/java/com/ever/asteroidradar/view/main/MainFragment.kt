package com.ever.asteroidradar.view.main

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.ever.asteroidradar.repository.AsteroidFilter
import com.ever.asteroidradar.repository.NasaRepository
import com.ever.asteroidradar.R
import com.ever.asteroidradar.adapter.AsteroidAdapter
import com.ever.asteroidradar.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by lazy {
        MainViewModelFactory(NasaRepository(requireContext().applicationContext))
            .create(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMainBinding.inflate(inflater)
        binding.lifecycleOwner = this

        binding.viewModel = viewModel
        binding.asteroidRecycler.adapter = AsteroidAdapter(AsteroidAdapter.AsteroidClickListener {
            viewModel.displayDetailScreen(it)
        })

        viewModel.refreshImageOfTheDay()

        viewModel.navigateToAsteroidDetails.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                findNavController().navigate(MainFragmentDirections.actionShowDetail(it))
                viewModel.doneNavigatingToDetail()
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_overflow_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onResume() {
        super.onResume()
        viewModel.refreshList()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.refreshFilter(
            when (item.itemId) {
                R.id.show_buy_menu -> {
                    AsteroidFilter.FAVORITE
                }
                R.id.show_rent_menu -> {
                    AsteroidFilter.TODAY
                }
                else -> {
                    AsteroidFilter.ALL
                }
            }
        )
        return true
    }
}
