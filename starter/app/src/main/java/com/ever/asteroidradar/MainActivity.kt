package com.ever.asteroidradar

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ever.asteroidradar.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
