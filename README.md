A little bit of context -
This is a project for the Udacity course with requirements to get the image of the day and asteroid list from the NASA API, show the list to the user based on a filter (today, next week, or the user favorites). This was done some time back, so it has some outdated dependencies.

Almost all of the code was done by me, there is some template code which was given by the instructor, mainly the parsing of the JSON (Which is strange since we can easily parse that automatically using Moshi, Gson or even Kotlin serializer).

This was done using Navigation, Databinding, Livedata, ViewModel, Retrofit, and Room.

I went through the code before sending it, and I see that there are many areas of opportunity:


* Proper model separation between Network and Database
* Parcelable is no longer needed while using navigation component
* Dependency injection - Hilt/Koin
* Automatic JSON parsing and mapping

Refactor:

* Constants out the object class
* On click events to view model
* Provide context from app level to database to avoid any further reference in fragments for initialization
* Kotlin flow would be a nice-to-have for status management

Other things that can be improved:

* TESTS!!!!
* App icon
* Translations
* Styles
* Dark mode
* Animations
* Proper error handling of failures from the backend

